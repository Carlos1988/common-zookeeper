name := """common-zookeeper"""

version := "1.0"

organization := "com.knowledge.base.zookeeper"

scalaVersion := "2.12.3"

// Change this to another test framework if you prefer
libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.4.9"

// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"


import knowledge.base.zookeeper.common.library.ZookeeperService

object ZookeeperServiceTest extends App {
  override def main(args: Array[String]) {
    val zks = new ZookeeperService("localhost:2181")
    
    try {
      val data = "the big black ball".getBytes
      if (!Option(zks.status("/thepath")).isDefined) 
        zks.create("/thepath", data)
      println(zks.getData("/thepath"))
    } finally {
     zks.close 
    }
  }
}
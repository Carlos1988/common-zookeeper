package knowledge.base.common.library.zookeeper

import org.apache.zookeeper.ZooKeeper
import org.apache.zookeeper.Watcher
import org.apache.zookeeper.WatchedEvent
import org.apache.zookeeper.Watcher.Event.KeeperState
import java.util.concurrent.CountDownLatch
import org.apache.zookeeper.ZooDefs
import org.apache.zookeeper.CreateMode
import org.apache.zookeeper.data.Stat
import scala.util.Try
import org.slf4j.Logger
import java.util.concurrent.TimeUnit


class ZookeeperService(host: String = "localhost:2181", sessionTimeOut: Int = 10000){
  private val connectedSignal = new CountDownLatch(1)
  private val zk = new ZooKeeper(host, sessionTimeOut, new Watcher {
         override def process(we: WatchedEvent): Unit = {
            if (we.getState() == KeeperState.SyncConnected) {
               connectedSignal.countDown();
            }
         }
  })
  connectedSignal.await(5, TimeUnit.SECONDS)
  
  def close = zk.close()
  def create(path: String, data: Array[Byte]): Unit = zk.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
  def status(path: String) = Some(zk.exists(path, true))
  def getData(path: String, stat: Stat = null) = Some(zk.getData(path, true, stat)).map{x => String.valueOf(x.map(_.toChar))}
  def getZookeeperInstance = zk
  
  override def finalize() = {
    try { zk.close() } catch {case e:Throwable => /*do nothing*/}
  }
}